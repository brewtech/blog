require "ckeditor"
#require "paperclip"
require "bootstrap-sass"
require "jquery-rails"
require "will_paginate"
module Blog
  class Engine < ::Rails::Engine
    isolate_namespace Blog

    config.generators do |g|
        g.test_framework :rspec,    :fixture => false
        g.fixture_replacement :factory_girl, :dir => 'spec/factories'
        g.assets false
        g.helper false
    end
  end
end
