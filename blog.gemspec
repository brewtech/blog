$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "blog/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "blog"
  s.version     = Blog::VERSION
  s.authors     = ["Evan Farrell"]
  s.email       = ["evan@brewtech.us"]
  s.homepage    = "http://brewtech.us"
  s.summary     = "The brewtech blog"
  s.description = "The brewtech blog."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 4.0.2"
  s.add_dependency "haml-rails"
  s.add_dependency "haml"
  s.add_dependency "ckeditor"
  s.add_dependency "paperclip"
  s.add_dependency "jquery-rails"
  s.add_dependency "will_paginate"
  s.add_dependency "bootstrap-sass", "~> 3.0.3.0"
  s.add_dependency "sass-rails"
  s.add_dependency "simple_form"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "faker"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "launchy"
  s.add_development_dependency "cucumber-rails"
  s.add_development_dependency "database_cleaner"
end
