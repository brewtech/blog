Blog::Engine.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  resources :posts, only: [:index, :show]
  resources :categories, only: [:show]
  resources :comments
  root "posts#index"
    namespace :blogadmin do
        resources :comments
        resources :posts
        resources :categories, only: [:update,:destroy,:index, :new, :create]
        root "posts#index"
    end
end
