class AddPostIdToBlogComments < ActiveRecord::Migration
  def change
    add_column :blog_comments, :post_id, :integer
  end
end
