class AddTitleToBlogCategories < ActiveRecord::Migration
  def change
    add_column :blog_categories, :title, :string
  end
end
