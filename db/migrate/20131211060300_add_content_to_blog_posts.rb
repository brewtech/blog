class AddContentToBlogPosts < ActiveRecord::Migration
  def change
    add_column :blog_posts, :content, :text
  end
end
