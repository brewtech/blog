    CKEDITOR.config.toolbar = [
        { name: 'document', items: ['Source', '-','Print','Find','Maximize'] },
        { name: 'insert', items: ['Image','Table','Link','SpecialChar','Iframe'] },
        { name: 'font', items: ['Styles', 'Font', 'FontSize']},
        '/',
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'TextColor']},
        { name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        { name: 'lists', items: ['NumberedList', 'BulletedList', '-', 'Blockquote']},
        { name: 'indent', items: ['Outdent', 'Indent']},
        ]

