require_dependency "blog/application_controller"

module Blog
  class PostsController < ApplicationController
      def index
          @posts = Post.where(publish: true).paginate(page: params[:page], per_page: 5)
      end
      def show
          @post = Post.find(params[:id])
          @comments = @post.comments.paginate(page: params[:page], per_page: 20)
          @comment = @post.comments.new
      end
  end
end
