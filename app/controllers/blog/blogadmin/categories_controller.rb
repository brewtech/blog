require_dependency "blog/application_controller"

module Blog
  class Blogadmin::CategoriesController < ApplicationController
      before_action :admin_user
      layout 'blog/dashboard'
      def index
          @categories = Category.all
      end
      def new
          @category = Category.new
      end
      def create
          @category = Category.new(category_params)
          if @category.save
              redirect_to admin_categories_path, success: "Category Created Successfully"
          else
              flash.now[:error] = "Could not create new Category"

          end
      end

      def destroy
          Category.find(params[:id]).destroy
          redirect_to admin_categories_path, success: "Deleted Successfully"
      end

      private
        def category_params
            params[:category].permit(:title)
        end
  end
end
