require_dependency "blog/application_controller"

module Blog
  class Blogadmin::CommentsController < ApplicationController
      before_action :admin_user
      layout 'blog/dashboard'
      def index
          @comments = Comment.paginate(page: params[:page], per_page: 25)
      end
      def destroy
          Comment.find(params[:id]).destroy
          redirect_to admin_comments_path
      end
  end
end
