require_dependency "blog/application_controller"

module Blog
  class Admin::PostsController < ApplicationController
      before_action :admin_user
      layout "blog/dashboard"
      def index
          @posts = Post.paginate(page: params[:page], per_page: 20)
      end
      def new
          @post = Post.new
      end
      def create
          @post = Post.new(post_params)
          if params[:save_as_draft]
              @post.publish = false
              if @post.save
                  flash.now[:success] = "Post Saved as Draft"
                  render :new
              else
                  flash.now[:error] = "Could not Save"
                  render :new
              end
          else
              @post.publish = true
              if @post.save
                  flash[:success] = "Post Published"
                  redirect_to admin_posts_path, success: "Post Published"
              else
                  flash.now[:error] = "Could not Publish"
                  render :new
              end
          end

      end

      def edit
          @post = Post.find(params[:id])
      end
      def update
          @post = Post.find(params[:id])
          if params[:save_as_draft]
              @post.publish = false
              if @post.update(post_params)
                  flash.now[:success] = "Updated Successfully"
                  render :edit
              else
                  flash.now[:error] = "Could not Save"
                  render :edit
              end
          else
              @post.publish = true
              if @post.update(post_params)
                  redirect_to admin_posts_path, success: "Updated Successfully"
              else
                  flash.now[:error] = "Could not Publish"
                  render :edit
              end
          end
      end
      
      def destroy
          Post.find(params[:id]).destroy
          redirect_to admin_posts_path
      end
      private

        def post_params
            params[:post].permit(:title, :content, :bootsy_image_gallery_id, {:category_ids => []})
        end

  end
end
