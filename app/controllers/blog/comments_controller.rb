require_dependency "blog/application_controller"

module Blog
  class CommentsController < ApplicationController
      before_action :signed_in_user, only: [:create, :new]
      def create
          @comment = Comment.new(comment_params)
          if @comment.save
              redirect_to post_path(@comment.post_id), success: "Comment Created."
          else
              flash.now[:error] = "Could not Post comment."
              render :show
          end
      end

      private
        def comment_params
            params[:comment].permit(:text,:post_id)
        end


  end
end
