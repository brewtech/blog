require_dependency "blog/application_controller"

module Blog
  class CategoriesController < ApplicationController
      def show
        @category = Category.find(params[:id])
        @posts = @category.posts
      end
  end
end
