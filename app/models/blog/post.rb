module Blog
  class Post < ActiveRecord::Base
      has_many :comments
      has_and_belongs_to_many :categories
      default_scope -> { order('created_at DESC')}
      validates :content, presence: true
      validates :title, presence: true, uniqueness: { case_sensitive: false }
  end
end
