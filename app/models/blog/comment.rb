module Blog
  class Comment < ActiveRecord::Base
      belongs_to :post
      default_scope -> {order('created_at DESC')}
      validates :text, presence: true, uniqueness: { scope: :post_id, case_sensitive: false }, length: { maximum: 200}
  end
end
