Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount Blog::Engine => "/blog"
end
