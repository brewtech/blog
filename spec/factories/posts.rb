# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post, class: Blog::Post do
    content "This is the content of the blog.  Lorem Ipsum"
    sequence :title do |n| 
        "My first blog post#{n}"
    end
    publish true

    factory :invalid_post, class: Blog::Post do
        title nil
    end
  end
  factory :category, class: Blog::Category do
    sequence :title do |n|
        "Homebrew#{n}"
    end
  end
  factory :comment, class: Blog::Comment do
    sequence :text do |n| 
        "I like this#{n}"
    end
    association :post
  end
end
