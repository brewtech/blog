require 'spec_helper'

describe Blog::Admin::PostsController do
    routes { Blog::Engine.routes }
    
    describe 'GET #new' do
        it "assigns a new Post to @post" do 
            get :new
            expect(assigns(:post)).to be_a_new(Blog::Post)
        end
        it "renders the :new template" do
            get :new
            expect(response).to render_template :new
        end
    end
    describe 'GET #edit' do
        it "assigns the requested post to @post" do
            post = FactoryGirl.create(:post)
            get :edit, id: post
            expect(assigns(:post)).to eq post
        end
        it "renders the :edit template" do
            post = FactoryGirl.create(:post)
            get :edit, id: post
            expect(response).to render_template :edit
        end
    end
    describe "POST #create" do
        context "with valid attributes" do
            it "saves the new post in the database" do
                expect{
                    post :create, post: FactoryGirl.attributes_for(:post)
                }.to change(Blog::Post, :count).by(1)
            end
            it "redirects to posts#index" do
                post :create, post: FactoryGirl.attributes_for(:post)
                expect(response).to redirect_to admin_posts_path
            end
            
        end

        context "with invalid attributes" do
            it "does not save the new post in the database" do
                expect{
                    post :create, post: FactoryGirl.attributes_for(:invalid_post)
                }.to_not change(Blog::Post, :count)
            end
            it "re-renders the :new template" do
                post :create, post: FactoryGirl.attributes_for(:invalid_post)
                expect(response).to render_template :new
            end
        end
    end
    describe 'PATCH #update' do
        before :each do
            @post = FactoryGirl.create(:post, title: "AWS or Rackspace")
        end
        context "with valid attributes" do
            it "located the requested @post" do
                patch :update, id: @post, post: FactoryGirl.attributes_for(:post)
                expect(assigns(:post)).to eq(@post)
            end
            it "changes @posts's attributes" do
                patch :update, id: @post, post: FactoryGirl.attributes_for(:post, title: "AWS first")
                @post.reload
                expect(@post.title).to eq("AWS first")
            end
            it "redirects to the posts table" do
                patch :update, id: @post, post: FactoryGirl.attributes_for(:post)
                expect(response).to redirect_to admin_posts_path
            end
        end

        context "with invalid attributes" do
            it "does not update the post" do
                patch :update, id: @post,
                  post: FactoryGirl.attributes_for(:post, content: "Hello there", title: nil)
                @post.reload
                expect(@post.content).to_not eq("Hello there")
                expect(@post.title).to_not be_nil
        end
            it "re-renders the #edit template" do
                patch :update, id: @post,
                    post: FactoryGirl.attributes_for(:invalid_post)
                expect(response).to render_template :edit
            end
        end
    end

    describe 'DELETE #destroy' do
        before :each do
            @post = FactoryGirl.create(:post)
        end
        it "deletes the post from the database" do
            expect{
                delete :destroy, id: @post
                }.to change(Blog::Post, :count).by(-1)
        end
        it "redirects to posts#index" do
            delete :destroy, id: @post
            expect(response).to redirect_to admin_posts_path
        end
    end

end
