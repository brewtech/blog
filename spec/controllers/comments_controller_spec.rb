require 'spec_helper'

describe Blog::Admin::CommentsController do
    routes { Blog::Engine.routes }

    describe 'DELETE #destroy' do
        before :each do
            @comment = FactoryGirl.create(:comment)
        end
        it "deletes the post from the database" do
            expect{
                delete :destroy, id: @comment
                }.to change(Blog::Comment, :count).by(-1)
        end
        it "redirects to posts#index" do
            delete :destroy, id: @comment
            expect(response).to redirect_to admin_comments_path
        end
    end
end
