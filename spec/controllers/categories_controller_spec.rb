require 'spec_helper'

describe Blog::Admin::CategoriesController do
    routes { Blog::Engine.routes }
    describe 'DELETE destroy' do
        before :each do
            @category = FactoryGirl.create(:category)
        end

        it "deletes the category" do
            expect { delete :destroy, id: @category
            }.to change(Blog::Category, :count).by(-1)
        end

        it "redirects to categories#index" do
            delete :destroy, id: @category
            expect(response).to redirect_to admin_categories_path
        end
    end

end
