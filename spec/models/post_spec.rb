require 'spec_helper'
describe Blog::Post do
    it "is valid with a title and content" do
        expect(FactoryGirl.build(:post)).to be_valid
    end
    it "is invalid without a title" do
        expect(FactoryGirl.build(:post, title: nil)).to have(1).errors_on(:title)
    end
    it "is invalid without content" do
        expect(FactoryGirl.build(:post, content: nil)).to have(1).errors_on(:content)
    end
    it "is invalid with a duplicate title" do
        test = FactoryGirl.create(:post, title: "Now I like Margie")
        expect(FactoryGirl.build(:post, title: "Now I like Margie")).to_not be_valid
    end
    it "is in reverse chronological order" do
        post = FactoryGirl.create(:post)
        post2 = FactoryGirl.create(:post)
        expect(Blog::Post.first).to eq(post2)
    end
   
end
