require 'spec_helper'

describe Blog::Category do
    it "is invalid with a duplicate category title" do
        Blog::Category.create(title: "Margie")
        category = Blog::Category.new(title: "Margie")
        expect(category).to have(1).errors_on(:title)
    end
    it "is invalid without a title" do
        category = Blog::Category.new(title: nil)
        expect(category).to have(1).errors_on(:title)
    end
end
