require 'spec_helper'

describe Blog::Comment do
    it "does not allow duplicate comments on one post" do
        post = FactoryGirl.build(:post)
        comment1 = FactoryGirl.create(:comment, text: "Test", post: post)
        comment2 = FactoryGirl.build(:comment, text: "Test", post: post)
        expect(comment2).to have(1).errors_on(:text)
    end
    it "allows two posts to have the same comment" do
        FactoryGirl.create(:comment, text: "Test")
        expect(FactoryGirl.build(:comment, text: "Test")).to be_valid
    end
    it "is invalid without text" do
        expect(FactoryGirl.build(:comment, text: nil)).to have(1).errors_on(:text)
    end
    it "is valid with text" do
        expect(FactoryGirl.build(:comment)).to be_valid
    end
    it "is in reverse chronological order" do
        comment = FactoryGirl.create(:comment)
        comment2 = FactoryGirl.create(:comment)
        expect(Blog::Comment.first).to eq(comment2)
    end
end
