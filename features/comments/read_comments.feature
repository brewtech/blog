Feature: read comments
    As a user
    In order to read helpful tips from other users
    I want to read comments

    Background:
        Given I have a post

    Scenario: Comment shows up 
        When I leave a comment
        And I visit the blog
        Then I should see a link "(1) Comment"

    Scenario: two comments
        When I leave two comments
        And I visit the blog
        Then I should see a link "(2) Comments"

    Scenario: No comments
        When I leave no comments
        And I visit the blog
        Then I should see a link "(0) Comments"

    Scenario: Click (x) Comments
        And I leave a comment
        When I visit the blog
        And I click "(1) Comment"
        Then I should not see a link "(1) Comment"

    Scenario: Click Title
        And I leave a comment
        When I visit the blog
        And I click the title "Second Post"
        Then I should not see a link "(1) Comment"

    Scenario: Read Comment
        And I leave a comment "You are the best"
        When I visit the blog
        And I click the title "Second Post"
        Then I should see the comment "You are the best"

    Scenario: Comments appear on muliple pages
        Given I have "21" Comments
        When I look at the blog
        And I click "(21) Comment"
        Then I should see the 20th comment
        And I should not see the 21th comment

        
