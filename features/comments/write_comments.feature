Feature: Write Comments
    As a user
    In order to write about the blog post
    I want to write a comment

    Scenario: Add Comment
        Given I have a post
        And I am on the Post's Page
        When I input a comment
        Then I should see the comment
