Feature: Remove Posts
    As an Owner
    In Order to remove foul comments
    I want to delete comments

    Scenario: Delete Comment
        Given I have a post
        And I leave a comment
        When I am on the Admin Dashboard
        And I go to the Comments page
        And I click on "Delete" 
        Then the comment should be gone
