Feature: Remove Categories
    As a blog owner
    In order to remove dumb categories
    I want to delete categories

    Scenario: Remove Category
        Given I create a new Category
        And I am on the Admin Dashboard
        When I go to the Categories page
        And I click on "Delete" 
        Then the Category should be gone

