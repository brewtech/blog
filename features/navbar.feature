Feature: Navbar
    As an Owner
    In order to Navigate
    I want to have a navbar

    Scenario: Blog link
    Given I am on the Admin page
    When I click on "Blog"
    Then I will see the Blog

    Scenario: Admin link
    Given I am on the Blog page
    When I click on "Admin Dashboard"
    Then I will see the Dashboard



