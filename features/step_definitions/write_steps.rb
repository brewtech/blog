Given /^there are no blog posts$/ do
    Blog::Post.delete_all
end

When(/^I create a new Post$/) do
    visit new_admin_post_path 
    fill_in "post[title]", with: 'Love:  Is it a good thing?'
    fill_in "post[content]", with: "I hate love sometimes.  I know it is awesome.  But I am in love with Becca, my best friend."
end

When(/^I publish it$/) do
    click_button "Publish Post"
end

Then(/^I should see "(.*?)"$/) do |content|
    page.should have_content(content)
end

Then(/^I should see the post I published$/) do
    visit root_path
    page.should have_content("Love: Is it a good thing?")
end

When(/^I save as a draft$/) do
    click_button "Save as Draft"
    response.should  == nil
end

Then(/^I should not see the post I saved$/) do
    visit root_path
    page.should_not have_content("Love: Is it a good thing?")
end

Then(/^the content should be in the editor$/) do
    page.should have_content("I hate love sometimes.  I know it is awesome.  But I am in love with Becca, my best friend.")
end

Given(/^I am on the Admin Dashboard$/) do
    visit admin_posts_path
end

When(/^I click "(.*?)"$/) do |link|
    click_link link
end

Then(/^I should be on the new post page$/) do
    page.should have_content "Create New Post"
end

