When (/^I go to the Comments page$/) do
    visit admin_comments_path
end

Then(/^the comment should be gone$/) do
    page.should_not have_content "ll:  Is it a good thing?"
end
