Given(/^I have a post called "(.*?)"$/) do |title|
    FactoryGirl.create(:post, title: title)
end

When(/^I visit the Admin posts view$/) do
    visit admin_posts_path
end

When(/^I click on "(.*?)"$/) do |button|
    click_link button
end

When(/^I change the title to "(.*?)"$/) do |title|
    fill_in "post[title]", with: title
    click_button "Publish Post"
end

Then(/^I should see "(.*?)" on the blog$/) do |title|
    visit root_path
    page.should have_content(title) 
end

When(/^I update the post to "(.*?)"$/) do |arg1|
    visit admin_posts_path
    click_link "Edit Post"
    fill_in "post[title]", with: title
    click_button "Publish Post"
end

Given(/^I have a post called "(.*?)" and it isn't published$/) do |title|
    FactoryGirl.create(:post, title: title, publish: false)
end

When(/^I update the post to "(.*?)" and save it$/) do |arg1|
    visit admin_posts_path
    click_link "Edit Post"
    fill_in "post[title]", with: title
    click_button "Save as Draft"
end

Then(/^I should not see "(.*?)"$/) do |title|
    visit root_path
    page.should_not have_content(title)
end

When(/^I update the post to "(.*?)" and publish it$/) do |title|
    visit admin_posts_path
    click_link 'Edit Post'
    fill_in "post[title]", with: title
    click_button "Publish Post"
end

When(/^visit the blog$/) do
    visit root_path
end



