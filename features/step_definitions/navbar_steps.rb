Given(/^I am on the Admin page$/) do
    visit admin_root_path
end

Then(/^I will see the Blog$/) do
    page.should have_title "Blog"
end

Given(/^I am on the Blog page$/) do
    visit root_path 
end

Then(/^I will see the Dashboard$/) do
    page.should have_title "Admin Dashboard"
end

