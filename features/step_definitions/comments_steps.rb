Given(/^I have a post$/) do
    Blog::Post.delete_all
    @post = FactoryGirl.create(:post, title: "Second Post")
end

When(/^I leave a comment$/) do
    @comment = FactoryGirl.create(:comment, post: @post)
end

Then(/^I should see a link "(.*?)"$/) do |comment|
    page.should have_content comment
end

When(/^I leave two comments$/) do
    FactoryGirl.create(:comment, text: "Hello", post: @post)
    FactoryGirl.create(:comment, text: "Hi", post: @post)
end

When(/^I leave no comments$/) do
    Blog::Comment.delete_all
end

Then(/^I should not see a link "(.*?)"$/) do |link|
    page.should_not have_content link
end

When(/^I click the title "(.*?)"$/) do |title|
    click_link title
end

When(/^I visit the blog$/) do
    visit root_path
end

Given(/^I am on the Post's Page$/) do
    visit post_path(@post.id)
end

When(/^I input a comment$/) do
    fill_in "comment[text]", with: "You are the most awesome person ever!"
    click_button "Add Comment" 
end

Then(/^I should see the comment$/) do
    page.should have_content "You are the most awesome person ever!"

end

Given(/^I leave a comment "(.*?)"$/) do |comment|
    FactoryGirl.create(:comment, text: comment, post: @post)
end

Then(/^I should see the comment "(.*?)"$/) do |comment|
    page.should have_content comment
end

Given(/^I have "(.*?)" Comments$/) do |number|
    @lastcomment = FactoryGirl.create(:comment, text: "You cannot see this", post: @post)
    @seecomment = FactoryGirl.create(:comment, text: "You can see this", post: @post)
    (number.to_i-2).times {
        FactoryGirl.create(:comment, post: @post)
    }

end

Then(/^I should see the (\d+)th comment$/) do |arg1|
  page.should have_content @seecomment.text
end

Then(/^I should not see the (\d+)th comment$/) do |arg1|
  page.should_not have_content @lastcomment.text
end

