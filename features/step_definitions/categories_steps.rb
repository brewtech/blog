Given(/^I have a Post$/) do
    FactoryGirl.create(:post)
end

Given(/^I have categories "(.*?)"$/) do |categories|
    categories.split(", ").each do |category|
        FactoryGirl.create(:category, title: category)
    end
end

When(/^I add categories "(.*?)"$/) do |categories|
    visit admin_posts_path
    click_link "Edit Post"
    categories.split(", ").each do |category|
        check category
    end
    click_button "Publish Post"
end

Then(/^the categories "(.*?)" should appear on the post$/) do |categories|
    categories.split(", ").each do |category|
        page.should have_content category
    end
end

Given(/^I have "(.*?)" post called "(.*?)" with Category "(.*?)"$/) do |num, titles, category|
    cat = FactoryGirl.create(:category, title: category)
    titles.split(", ").each do |title| 
        FactoryGirl.create(:post, title: title, :categories => [cat]) 
    end
end

When(/^I click on the Category "(.*?)"$/) do |category|
    first(:link, category).click
end

Then(/^I should see a "(.*?)"$/) do |titles|
    titles.split(", ").each do |title| 
        page.should have_content title
    end
end


Then(/^I should not see the post "(.*?)"$/) do |title|
    page.should_not have_content title
end

When(/^I go to the Categories page$/) do
    click_link "Categories"
end

When(/^create a new Category$/) do
    click_link "Create New Category"
    fill_in "category[title]", with: "Kittens"
    click_button "Create New Category"
end

Then(/^I will see the Category on the New Page Post$/) do
    visit new_admin_post_path
    page.should have_content "Kittens"
end

Given(/^I have no Categories$/) do
    Blog::Category.delete_all
end

When(/^I create a new Category$/) do
    @category = FactoryGirl.create(:category, title: "Puppies") 
end

Then(/^I will see the Category on the Admin page$/) do
    visit admin_categories_path
   page.should have_content "Puppies" 
end


Then(/^the category should be gone$/) do
    page.should_not have_content category.title
end
