Given(/^there is a blog post called "(.*?)"$/) do |title|
    FactoryGirl.create(:post, title: title)
end

When(/^I look at the blog$/) do
    visit root_path
end

Then(/^the post "(.*?)" should be visible on the blog$/) do |title|
    page.should have_content(title)
end

Given(/^there is a blog post with content "(.*?)"$/) do |content|
    FactoryGirl.create(:post, content: content)
end

Then(/^the content "(.*?)" should be visible on the blog$/) do |content|
    page.should have_content(content)
end

When(/^click on the title "(.*?)"$/) do |title|
    click_link title
end

Then(/^the post "(.*?)" should not be visible on the blog$/) do |content|
    page.should_not have_content content
end

Given(/^I have a long blog post over (\d+) words$/) do |words|
    content_final = String.new
    8.times {
        content = Faker::Lorem.words(num = 300).each_slice(150).to_a.join(' ')
        content_final << content << "</p>"
    }
    content_final << "Hmm, I wonder what will happen with Margie and me?"
    
    FactoryGirl.create(:post, content: content_final)
end

Then(/^I should not see the end of the post$/) do
page.should_not have_content "Hmm, I wonder what will happen with Margie and me?"
end


Given(/^I have "(.*?)" blog posts$/) do |number|
    @lastpost = FactoryGirl.create(:post, title: "I should not see this")
    @seepost = FactoryGirl.create(:post, title: "I should see this")
    (number.to_i-2).times {
        FactoryGirl.create(:post)
    }

end

Then(/^I should see the (\d+)th post$/) do |arg1|
    page.should have_content @seepost.title
end

Then(/^I should not see the (\d+)th post$/) do |arg1|
    page.should_not have_content @lastpost.title
end

