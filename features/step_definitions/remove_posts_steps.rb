When(/^I delete the Post$/) do
    click_button "Delete"
end

Then(/^the post should be gone$/) do
    page.should_not have_content @post.content
end
Then(/^the Category should be gone$/) do
    page.should_not have_content @category.title
end

