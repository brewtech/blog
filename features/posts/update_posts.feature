Feature: Update posts
    As an Admin
    In order to change typos and errors
    I want to edit my posts

    Scenario: Update posts
        Given I have a post called "Freiburg"
        When I visit the Admin posts view
        And I click on "Edit Post"
        And I change the title to "Freiburg:  Where I met some of my best friends"
        Then I should see "Freiburg: Where I met some of my best friends" on the blog

    Scenario: Update posts show notice
        Given I have a post called "Freiburg"
        When I update the post to "Freibug:  A beautiful city" and save it
        Then I should see "Updated Successfully"
    
    Scenario: Update and save
        Given I have a post called "Freiburg" and it isn't published
        When I update the post to "Freiburg: A beautiful city" and save it
        Then I should not see "Freiburg: A beautiful city"
 
    Scenario: Update and publish
        Given I have a post called "Freiburg" and it isn't published
        When I update the post to "Freiburg: land of Neuer Susse Wein" and publish it
        And visit the blog
        Then I should see "Freiburg: land of Neuer Susse Wein"
