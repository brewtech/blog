Feature:
    As a blog reader
    So that I can learn
    I want to read blog posts

    Scenario: "title visible"
        Given there is a blog post called "My Post"
        When I look at the blog
        Then the post "My Post" should be visible on the blog

    Scenario: "Content visible"
        Given there is a blog post with content "This is the content of the post"
        When I look at the blog
        Then the content "This is the content of the post" should be visible on the blog

    Scenario: "Multiple posts on page"
        Given there is a blog post called "Love sucks"
        And there is a blog post called "Becca"
        When I look at the blog
        Then the post "Love sucks" should be visible on the blog
        And the post "Becca" should be visible on the blog

   Scenario: View one Post
        Given there is a blog post called "Biking"
        And there is a blog post called "Running"
        When I look at the blog
        And click on the title "Biking"
        Then the post "Biking" should be visible on the blog
        And the post "Running" should not be visible on the blog

   Scenario: View part of Post
        Given I have a long blog post over 800 words
        When I look at the blog
        Then I should not see the end of the post
        And I should see "Continued..."

   Scenario: Posts appear on multiple pages
        Given I have "6" blog posts
        When I look at the blog
        Then I should see the 5th post
        And I should not see the 6th post
        And I should see a "Previous Posts" 
