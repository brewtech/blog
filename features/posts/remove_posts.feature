Feature: Remove Posts
    As a blog owner
    In order to remove dumb posts
    I want to delete posts

    Scenario: Remove Post
        Given I have a post
        When I am on the Admin Dashboard
        And I delete the Post
        Then the post should be gone

