Feature: 
    As a blog owner
    In order to maximize potential
    I want to create posts on my blog

    Scenario: Publish Post 
        Given there are no blog posts
        When I create a new Post
        And I publish it
        Then I should see "Post Published"
        And I should see the post I published

    Scenario:  Draft Post
        Given there are no blog posts
        When I create a new Post
        And I save as a draft 
        Then I should see "Post Saved as Draft"
        And I should not see the post I saved
    
    Scenario: reload same content
        Given I create a new Post
        When I save as a draft
        Then the content should be in the editor

    Scenario: Click New Button
        Given I am on the Admin Dashboard
        When I click "New Post"
        Then I should be on the new post page
