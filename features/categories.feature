Feature: Add categories to Posts
    As an Owner
    In order to Catalog Posts
    I want to add Categories to Posts
    
    Scenario:  Add category to Post
        Given I have a Post
        And I have categories "Beer, Love, Running"
        When I add categories "Love, Running"
        And I am on the Blog page
        Then the categories "Love, Running" should appear on the post
    
    Scenario: Find similiar posts
        Given I have "3" post called "Feierling, Rothaus, Ganter" with Category "Beer"
        And I am on the Blog page
        When I click on the Category "Beer"
        Then I should see a "Feierling, Rothaus, Ganter"

    Scenario: Find similiar posts
        Given I have "1" post called "Feierling" with Category "Beer"
        And I have "1" post called "Poperigne" with Category "Hops"
        And I am on the Blog page
        When I click on the Category "Beer"
        Then I should not see the post "Poperigne"
    
    Scenario: Create new category
        Given I am on the Admin Dashboard
        When I go to the Categories page
        And create a new Category
        Then I will see the Category on the New Page Post

    Scenario: View all Categories
        Given I have no Categories
        When I create a new Category
        Then I will see the Category on the Admin page

    Scenario: View Categories on Post page
        Given I have a Post
        And I have categories "Beer, Love, Running"
        When I add categories "Love, Running"
        And I am on the Blog page
        And click on the title "My first blog post"
        Then the categories "Love, Running" should appear on the post

